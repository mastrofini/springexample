package it.uniroma2.isssr.examples;

import org.springframework.stereotype.Repository;

public interface MyEntityRepositoryCustom {
    public void customMethod();
}